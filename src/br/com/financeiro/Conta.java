package br.com.financeiro;

import br.com.estados.conta.Negativo;
import br.com.estados.conta.Positivo;
import br.com.interfaces.EstadoDaConta;
import java.util.Calendar;

public class Conta
{
    private String titular;
    private double saldo;
    private String agencia;
    private String numeroConta;
    private Calendar aberturaConta;
    private EstadoDaConta estado;
    
    public Conta()
    {
        estado = new Positivo();
    }
    
    public double getSaldo() 
    {
        return saldo;
    }

    public void deposita(double valor)
    {
        estado.deposita(this, valor);
    }
    
    public void saca(double valor)
    {
        estado.saca(this, valor);
    }
    
    public String getTitular() 
    {
        return titular;
    }

    public void setTitular(String titular) 
    {
        this.titular = titular;
    }

    @Override
    public String toString() 
    {
        return "[ Titular : " + titular + "\nSaldo : " + saldo +  " ]";
    }

    public String getAgencia() 
    {
        return agencia;
    }

    public String getNumeroConta() 
    {
        return numeroConta;
    }

    public void setSaldo(double saldo) 
    {
        this.saldo = saldo;
        if(saldo < 0) this.estado = new Negativo();
        else this.estado = new Positivo();
    }

    public void setAgencia(String agencia) 
    {
        this.agencia = agencia;
    }

    public void setNumeroConta(String numeroConta) 
    {
        this.numeroConta = numeroConta;
    }

    public Calendar getAberturaConta() 
    {
        return aberturaConta;
    }

    public void setAberturaConta(Calendar aberturaConta) 
    {
        this.aberturaConta = aberturaConta;
    }

    public EstadoDaConta getEstado() 
    {
        return estado;
    }
    
    public void setEstado(EstadoDaConta estado) 
    {
        this.estado = estado;
    }
}
