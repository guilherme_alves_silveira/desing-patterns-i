package br.com.financeiro;

public class Banco 
{
    private int id;
    private String nome;
    private String telefone;
    private String endereco;
    private String email;

    public int getId() 
    {
        return id;
    }

    public String getNome() 
    {
        return nome;
    }

    public String getTelefone() 
    {
        return telefone;
    }

    public String getEndereco() 
    {
        return endereco;
    }

    public String getEmail() 
    {
        return email;
    }

    public void setId(int id) 
    {
        this.id = id;
    }

    public void setNome(String nome) 
    {
        this.nome = nome;
    }

    public void setTelefone(String telefone) 
    {
        this.telefone = telefone;
    }

    public void setEndereco(String endereco) 
    {
        this.endereco = endereco;
    }

    public void setEmail(String email) 
    {
        this.email = email;
    }
}
