package br.com.financeiro;

import br.com.estados.orcamento.EmAprovacao;
import br.com.interfaces.EstadoDoOrcamento;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Orcamento
{
    private double valor;
    private EstadoDoOrcamento estado;
    private final List<Item> itens;
    
    public Orcamento(double valor)
    {
        this.valor = valor;
        itens = new ArrayList<>();
        estado = new EmAprovacao();
    }

    public double getValor()
    {
        return valor;
    }

    public void setValor(double valor) 
    {
        this.valor = valor;
    }

    public List<Item> getItens() 
    {
        return Collections.unmodifiableList(itens);
    }

    public void adicionaItem(Item item) 
    {
        this.itens.add(item);
        this.valor += item.getValor();
    }

    public void aprova() 
    {
        estado.aprovado(this);
    }

    public void reprova()
    {
        estado.reprovado(this);
    }

    public void finaliza() 
    {
        estado.finalizado(this);
    }

    public void aplicaDesconto()
    {
        estado.aplicaDescontoExtra(this);
    }
    
    public void setEstado(EstadoDoOrcamento estado) 
    {
        this.estado = estado;
    }
}
