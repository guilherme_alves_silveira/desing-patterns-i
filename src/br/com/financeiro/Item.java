package br.com.financeiro;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Item 
{
    private String descricao;
    private double valor;
    private final List<Item> itensCasados;
    
    public Item(String descricao, double valor)
    {
        this.descricao = descricao;
        this.valor = valor;
        this.itensCasados = new ArrayList<>();
    }

    public String getDescricao() 
    {
        return descricao;
    }

    public double getValor()
    {
        return valor;
    }

    public void setDescricao(String descricao) 
    {
        this.descricao = descricao;
    }

    public void setValor(double valor) 
    {
        this.valor = valor;
    }
    
    public List<Item> getItensCasados()
    {
        return Collections.unmodifiableList(itensCasados);
    }

    public void adicionaItemCasado(Item item)
    {
        this.itensCasados.add(item);
    }
}
