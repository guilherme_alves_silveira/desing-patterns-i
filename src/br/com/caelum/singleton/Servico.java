package br.com.caelum.singleton;

public class Servico
{
    private static String nome;
    
    protected Servico(String nome)
    {
        this.nome = nome;
    }
    
    public String getNome()
    {
        if(nome == null)
        {
            nome = "";
        }
        return nome;
    }
}
