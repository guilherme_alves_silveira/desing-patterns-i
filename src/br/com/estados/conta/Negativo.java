package br.com.estados.conta;

import br.com.financeiro.Conta;
import br.com.interfaces.EstadoDaConta;

public class Negativo implements EstadoDaConta
{
    @Override
    public void saca(Conta conta, double valor) 
    {
        throw new RuntimeException("Não pode haver saque com saldo negativo.");
    }
    
    @Override
    public void deposita(Conta conta, double valor) 
    {
        conta.setSaldo(conta.getSaldo() + (valor * 0.95));
        if(conta.getSaldo() > 0) conta.setEstado(new Positivo());
    }
    
    @Override
    public String toString() 
    {
        return "NEGATIVO";
    }
}
