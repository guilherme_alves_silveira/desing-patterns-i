package br.com.estados.conta;

import br.com.financeiro.Conta;
import br.com.interfaces.EstadoDaConta;

public class Positivo implements EstadoDaConta
{
    @Override
    public void deposita(Conta conta, double valor) 
    {
        conta.setSaldo(conta.getSaldo() + (valor * 0.98));
        if(conta.getSaldo() < 0) conta.setEstado(new Negativo());
    }

    @Override
    public void saca(Conta conta, double valor) 
    {
        conta.setSaldo(conta.getSaldo() - valor);
        if(conta.getSaldo() < 0) conta.setEstado(new Negativo());
    }
    
    @Override
    public String toString() 
    {
        return "POSITIVO";
    }
}
