package br.com.estados.orcamento;

import br.com.financeiro.Orcamento;
import br.com.interfaces.EstadoDoOrcamento;

public class Aprovado implements EstadoDoOrcamento
{
    private boolean isDescontoRealizado = false;
    
    @Override
    public void aplicaDescontoExtra(Orcamento orcamento) 
    {
        if(!isDescontoRealizado)
        {
            double valor = orcamento.getValor() - orcamento.getValor() * 0.02;
            orcamento.setValor(valor);
            isDescontoRealizado = true;
        }
        else
        {
            throw new RuntimeException("O desconto não pode ser realizado mais de uma vez.");
        }
    }
    
    @Override
    public void emAprovacao(Orcamento orcamento)
    {
        throw new RuntimeException("Estado já aprovado não pode voltar em processo de aprovação.");
    }

    @Override
    public void aprovado(Orcamento orcamento) 
    {
        throw new RuntimeException("Estado já aprovado.");
    }

    @Override
    public void reprovado(Orcamento orcamento) 
    {
        orcamento.setEstado(new Reprovado());
    }

    @Override
    public void finalizado(Orcamento orcamento) 
    {
        orcamento.setEstado(new Finalizado());
    }
}
