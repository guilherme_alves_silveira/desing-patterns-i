package br.com.estados.orcamento;

import br.com.financeiro.Orcamento;
import br.com.interfaces.EstadoDoOrcamento;

public class Reprovado implements EstadoDoOrcamento
{
    @Override
    public void aplicaDescontoExtra(Orcamento orcamento) 
    {
        throw new RuntimeException("Estado reprovado não pode receber desconto extra.");
    }
    
    @Override
    public void emAprovacao(Orcamento orcamento) 
    {
        throw new RuntimeException("Estado não pode voltar em processo de aprovação.");
    }

    @Override
    public void aprovado(Orcamento orcamento) 
    {
        throw new RuntimeException("Estado já reprovado.");
    }

    @Override
    public void reprovado(Orcamento orcamento) 
    {
        throw new RuntimeException("Estado não pode ser reprovado novamente.");
    }

    @Override
    public void finalizado(Orcamento orcamento) 
    {
        orcamento.setEstado(new Finalizado());
    }
}