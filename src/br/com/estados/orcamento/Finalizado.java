package br.com.estados.orcamento;

import br.com.financeiro.Orcamento;
import br.com.interfaces.EstadoDoOrcamento;

public class Finalizado implements EstadoDoOrcamento
{
    @Override
    public void aplicaDescontoExtra(Orcamento orcamento) 
    {
        throw new RuntimeException("Estado já finalizado não pode receber descontos.");
    }
    
    @Override
    public void emAprovacao(Orcamento orcamento) 
    {
        throw new RuntimeException("Estado já finalizado.");
    }

    @Override
    public void aprovado(Orcamento orcamento) 
    {
        throw new RuntimeException("Estado já finalizado.");
    }

    @Override
    public void reprovado(Orcamento orcamento) 
    {
        throw new RuntimeException("Estado já finalizado.");
    }

    @Override
    public void finalizado(Orcamento orcamento)
    {
        throw new RuntimeException("Estado já finalizado.");
    }
}