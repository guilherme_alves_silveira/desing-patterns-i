package br.com.estados.orcamento;

import br.com.financeiro.Orcamento;
import br.com.interfaces.EstadoDoOrcamento;

public class EmAprovacao implements EstadoDoOrcamento
{
    private boolean isDescontoRealizado = false;
    
    @Override
    public void aplicaDescontoExtra(Orcamento orcamento) 
    {
        if(!isDescontoRealizado)
        {
            double valor = orcamento.getValor() - orcamento.getValor() * 0.05;
            orcamento.setValor(valor);
            isDescontoRealizado = true;
        }
        else
        {
            throw new RuntimeException("O desconto não pode ser realizado mais de uma vez.");
        }
    }
    
    @Override
    public void emAprovacao(Orcamento orcamento) 
    {
        orcamento.setEstado(new EmAprovacao());
    }

    @Override
    public void aprovado(Orcamento orcamento)
    {
        orcamento.setEstado(new Aprovado());
    }

    @Override
    public void reprovado(Orcamento orcamento) 
    {
        orcamento.setEstado(new Reprovado());
    }

    @Override
    public void finalizado(Orcamento orcamento)
    {
        throw new RuntimeException("Estado em aprovação, não pode ser finalizado.");
    }
}
