package br.com.impostos;

import br.com.financeiro.Orcamento;
import br.com.abstratos.Imposto;

public class ICCC extends Imposto
{
    public ICCC(){}
    
    public ICCC(Imposto outroImposto)
    {
        super(outroImposto);
    }
    
    @Override
    public double CalculaImposto(Orcamento orcamento) 
    {
        double valor = orcamento.getValor();
        
        if(valor >= 1000 && valor <= 3000)
        {
            return valor * 0.07 + CalculaOutroImposto(orcamento);
        }
        else if(valor > 3000)
        {
            return valor * 0.08 + 30 + CalculaOutroImposto(orcamento);
        }
        
        return valor * 0.05;
    }
}
