package br.com.impostos;

import br.com.abstratos.Imposto;
import br.com.abstratos.TemplateDeImpostoCondicional;
import br.com.financeiro.Orcamento;

public class ICPP extends TemplateDeImpostoCondicional
{
    private final double valor = 500.0;
    
    public ICPP(){}
    
    public ICPP(Imposto outroImposto)
    {
        super(outroImposto);
    }
    
    @Override
    protected boolean deveUsarMaximaTaxacao(Orcamento orcamento) 
    {
        return orcamento.getValor() > valor;
    }

    @Override
    protected double maximaTaxacao(Orcamento orcamento)
    {
        return orcamento.getValor() * 0.07;
    }

    @Override
    protected double minimaTaxacao(Orcamento orcamento) 
    {
        return orcamento.getValor() * 0.05;
    }
}
