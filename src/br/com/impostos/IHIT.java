package br.com.impostos;

import br.com.abstratos.TemplateDeImpostoCondicional;
import br.com.financeiro.Item;
import br.com.financeiro.Orcamento;

public class IHIT extends TemplateDeImpostoCondicional
{
    @Override
    protected boolean deveUsarMaximaTaxacao(Orcamento orcamento) 
    {
        return itensComMesmoNome(orcamento);
    }

    @Override
    protected double maximaTaxacao(Orcamento orcamento) 
    {
        return orcamento.getValor() * 0.13 + 100;
    }

    @Override
    protected double minimaTaxacao(Orcamento orcamento) 
    {
        return orcamento.getValor() * (orcamento.getItens().size() * 0.01);
    }
    
    private boolean itensComMesmoNome(Orcamento orcamento)
    {
        for(int it = 0;it < orcamento.getItens().size();it ++)
        {
            Item item = orcamento.getItens().get(it);
            for(int i = 0;i < orcamento.getItens().size();i++)
            {
                Item outro = orcamento.getItens().get(i);
                if(!(i == it) && item.getDescricao().equals(outro.getDescricao()))
                {
                    return true;
                }
            }
        }

        return false;
    }
}
