package br.com.impostos;

import br.com.abstratos.Imposto;
import br.com.abstratos.TemplateDeImpostoCondicional;
import br.com.financeiro.Orcamento;

public class IKCV extends TemplateDeImpostoCondicional
{
    private final double valor = 500.0;
    
    public IKCV(){}
    
    public IKCV(Imposto outroImposto)
    {
        super(outroImposto);
    }
    
    @Override
    protected boolean deveUsarMaximaTaxacao(Orcamento orcamento) 
    {
        return orcamento.getValor() > valor && possuiItemComValorAcimaDe100(orcamento);
    }

    @Override
    protected double maximaTaxacao(Orcamento orcamento)
    {
        return orcamento.getValor() * 0.10;
    }

    @Override
    protected double minimaTaxacao(Orcamento orcamento) 
    {
        return orcamento.getValor() * 0.06;
    }
    
    private boolean possuiItemComValorAcimaDe100(Orcamento orcamento)
    {
        return orcamento.getItens().stream().anyMatch((item) -> (item.getValor() > 100));
    }
}
