package br.com.impostos;

import br.com.financeiro.Orcamento;
import br.com.abstratos.Imposto;

public class ICMS extends Imposto
{
    public ICMS(){}
    
    public ICMS(Imposto outroImposto)
    {
        super(outroImposto);
    }
    
    @Override
    public double CalculaImposto(Orcamento orcamento) 
    {
        return orcamento.getValor() * 0.05 + 50 + CalculaOutroImposto(orcamento);
    }
}
