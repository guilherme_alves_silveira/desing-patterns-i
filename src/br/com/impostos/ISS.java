package br.com.impostos;

import br.com.financeiro.Orcamento;
import br.com.abstratos.Imposto;

public class ISS extends Imposto
{
    public ISS(){}
    
    public ISS(Imposto outroImposto)
    {
        super(outroImposto);
    }
    
    @Override
    public double CalculaImposto(Orcamento orcamento) 
    {
        return orcamento.getValor() * 0.06 + CalculaOutroImposto(orcamento);
    }
}
