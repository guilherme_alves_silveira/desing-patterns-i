package br.com.impostos;

import br.com.abstratos.Imposto;
import br.com.financeiro.Orcamento;

public class IAV extends Imposto
{
    public IAV(){}
    
    public IAV(Imposto outroImposto)
    {
        super(outroImposto);
    }
    
    @Override
    public double CalculaImposto(Orcamento orcamento)
    {
        return orcamento.getValor() * 0.2 + CalculaOutroImposto(orcamento);
    }
}
