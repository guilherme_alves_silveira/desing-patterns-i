package br.com.requisicao;

import br.com.enums.Formato;
import br.com.financeiro.Conta;
import br.com.financeiro.Requisicao;
import br.com.interfaces.RetornaRequisicao;

public class RetornaXML implements RetornaRequisicao
{
    private RetornaRequisicao proximo;
    
    @Override
    public String imprimeFormato(Requisicao requisicao, Conta conta) 
    {
        if(requisicao.getFormato().equals(Formato.XML))
        {
            String formatoRetornado = "<xml>" + conta.toString() + "</xml>";
            return formatoRetornado;
        }
        else
        {
            return proximo.imprimeFormato(requisicao, conta);
        }
    }

    @Override
    public void setProximo(RetornaRequisicao proximo) 
    {
        this.proximo = proximo;
    }
}
