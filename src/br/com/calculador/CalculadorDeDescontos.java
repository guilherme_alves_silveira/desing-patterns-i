package br.com.calculador;

import br.com.descontos.DescontoParaMaisDeCincoItens;
import br.com.descontos.DescontoPorVendaCasada;
import br.com.descontos.DescontoPraMaisDeQuinhentosValor;
import br.com.descontos.SemDesconto;
import br.com.financeiro.Orcamento;

public class CalculadorDeDescontos 
{
    public double RetornaDesconto(Orcamento orcamento)
    {
        DescontoParaMaisDeCincoItens descontoDez = new DescontoParaMaisDeCincoItens();
        DescontoPraMaisDeQuinhentosValor descontoQuinentos = new DescontoPraMaisDeQuinhentosValor();
        DescontoPorVendaCasada porVendaCasada = new DescontoPorVendaCasada();
        SemDesconto semDesconto = new SemDesconto();
        
        descontoDez.setProximo(descontoQuinentos);
        descontoQuinentos.setProximo(porVendaCasada);
        porVendaCasada.setProximo(semDesconto);
        
        return descontoDez.desconto(orcamento);
    }
}
