package br.com.calculador;

import br.com.financeiro.Conta;
import br.com.financeiro.Requisicao;
import br.com.requisicao.RetornaCSV;
import br.com.requisicao.RetornaPorcento;
import br.com.requisicao.RetornaXML;

public class CalculaRetornoDeRequisicao 
{
    public String RetornaTipoDaRequisicao(Requisicao requisicao, Conta conta)
    {
        if(requisicao.getFormato() == null)
        {
            throw new IllegalArgumentException("Formato não encontrado, por favor solicite um formato válido.");
        }
        
        RetornaCSV retornaCSV = new RetornaCSV();
        RetornaPorcento retornaPorcento = new RetornaPorcento();
        RetornaXML retornaXML = new RetornaXML();
        
        retornaCSV.setProximo(retornaPorcento);
        retornaPorcento.setProximo(retornaXML);

        return retornaCSV.imprimeFormato(requisicao, conta); 
    }
}
