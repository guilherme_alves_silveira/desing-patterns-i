package br.com.calculador;

import br.com.financeiro.Orcamento;
import br.com.abstratos.Imposto;

public class CalculadorDeImpostos 
{
    public void CalculaImposto(Orcamento orcamento, Imposto imposto, String nomeImposto)
    {
        System.out.println(nomeImposto + " : " + imposto.CalculaImposto(orcamento));
    }
}
