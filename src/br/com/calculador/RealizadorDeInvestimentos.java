package br.com.calculador;

import br.com.interfaces.Investidor;

public class RealizadorDeInvestimentos
{
    public double RetornaInvestimento(Investidor investidor, double valor)
    {
        double valorInvestido = investidor.valorInvestido(valor);
        return valorInvestido * 0.75;
    }
}
