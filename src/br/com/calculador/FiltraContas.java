package br.com.calculador;

import br.com.abstratos.Filtro;
import br.com.filtros.conta.DataAberturaMesNoCorrente;
import br.com.filtros.conta.SaldoComMaisDe500Mil;
import br.com.filtros.conta.SaldoComMenosDe100;
import br.com.filtros.conta.SemFiltro;
import br.com.filtros.conta.TitularComNomesExtenso;
import br.com.financeiro.Conta;
import java.util.Collection;
import java.util.List;

public class FiltraContas 
{
    public Collection<Conta> FiltraAsContas(List<Conta> contas)
    {
        Filtro sf = new SemFiltro();
        Filtro scmd100 = new SaldoComMenosDe100(sf);
        Filtro scmd500mil = new SaldoComMaisDe500Mil(scmd100);
        Filtro damnc = new DataAberturaMesNoCorrente(scmd500mil);
        Filtro tcne = new TitularComNomesExtenso(damnc);
        
        return tcne.filtra(contas);
    }
}
