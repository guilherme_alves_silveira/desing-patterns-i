package br.com.calculador;

import br.com.abstratos.TemplateDeRelatorio;
import br.com.enums.TipoRelatorio;
import br.com.financeiro.Banco;
import br.com.financeiro.Conta;
import br.com.relatorio.RelatorioComplexo;
import br.com.relatorio.RelatorioSimples;
import java.util.List;

public class SolicitadorDeRelatorios 
{
    public String solicitaRelatorioInformado(Banco banco, List<Conta> contas,TipoRelatorio tipoRelatorio)
    {
        if(!tipoRelatorio.equals(TipoRelatorio.COMPLEXO) 
           && !tipoRelatorio.equals(TipoRelatorio.SIMPLES))
        {        
            throw new IllegalArgumentException("O relatório solicitado não existe.");
        }
        
        TemplateDeRelatorio simples = new RelatorioSimples();
        TemplateDeRelatorio complexo = new RelatorioComplexo();
        simples.setProximo(complexo);

        return simples.RetornaRelatorio(banco, contas, tipoRelatorio);
    }
}
