package br.com.filtros.conta;

import br.com.abstratos.Filtro;
import br.com.financeiro.Conta;
import java.util.Calendar;

public class DataAberturaMesNoCorrente extends Filtro
{
    public DataAberturaMesNoCorrente(){}
    
    public DataAberturaMesNoCorrente(Filtro proximo)
    {
        super(proximo);
    }
    
    @Override
    public boolean isValido(Conta conta) 
    {
        Calendar dataCorrente = Calendar.getInstance();
        dataCorrente.setTimeInMillis(System.currentTimeMillis());
        return conta.getAberturaConta().getTimeInMillis() >= dataCorrente.getTimeInMillis();
    }
}
