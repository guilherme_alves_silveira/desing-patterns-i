package br.com.filtros.conta;

import br.com.abstratos.Filtro;
import br.com.financeiro.Conta;

public class SaldoComMenosDe100 extends Filtro
{
    public SaldoComMenosDe100(){}
    
    public SaldoComMenosDe100(Filtro proximo)
    {
        super(proximo);
    }
    
    @Override
    public boolean isValido(Conta conta) 
    {
        return conta.getSaldo() < 100;
    }
}
