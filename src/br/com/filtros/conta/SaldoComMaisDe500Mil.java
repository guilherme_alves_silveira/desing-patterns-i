package br.com.filtros.conta;

import br.com.abstratos.Filtro;
import br.com.financeiro.Conta;

public class SaldoComMaisDe500Mil extends Filtro
{
    public SaldoComMaisDe500Mil(){}
    
    public SaldoComMaisDe500Mil(Filtro proximo)
    {
        super(proximo);
    }
    
    @Override
    public boolean isValido(Conta conta) 
    {
        return conta.getSaldo() > 500000;
    }
}