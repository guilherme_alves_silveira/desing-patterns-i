package br.com.filtros.conta;

import br.com.abstratos.Filtro;
import br.com.financeiro.Conta;

public class TitularComNomesExtenso extends Filtro
{
    public TitularComNomesExtenso(){}
    
    public TitularComNomesExtenso(Filtro proximo)
    {
        super(proximo);
    }
    
    @Override
    public boolean isValido(Conta conta) 
    {
        return conta.getTitular().length() < 5;
    }
}
