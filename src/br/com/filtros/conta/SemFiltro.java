package br.com.filtros.conta;

import br.com.abstratos.Filtro;
import br.com.financeiro.Conta;

public class SemFiltro extends Filtro
{
    @Override
    public boolean isValido(Conta conta) 
    {
        return false;
    }
}
