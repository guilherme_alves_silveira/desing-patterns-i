package br.com.relatorio;

import br.com.abstratos.TemplateDeRelatorio;
import br.com.financeiro.Banco;
import br.com.financeiro.Conta;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class RelatorioComplexo extends TemplateDeRelatorio
{
    @Override
    protected void cabecalho(Banco banco) 
    {
        aparenciaRelatorio += "BANCO....:" + banco.getNome() + "\tTELEFONE...:" + banco.getTelefone() + "\n";
        aparenciaRelatorio += "ENDEREÇO.:" + banco.getEndereco() + "\n";
    }

    @Override
    protected void corpo(List<Conta> contas) 
    {
        for(Conta conta : contas) 
        {
            aparenciaRelatorio += "TITULAR...:" + conta.getTitular() + "\tSALDO....:" + conta.getSaldo() + "\n";
            aparenciaRelatorio += "AGÊNCIA...:" + conta.getAgencia() + "\tN° CONTA.: " + conta.getNumeroConta() + "\n";
        }
    }

    @Override
    protected void rodape(Banco banco) 
    {
        aparenciaRelatorio += "E-MAIL...:" + banco.getEmail() + "\tDATA...:" + Calendar.getInstance(Locale.getDefault());
    }
}
