package br.com.relatorio;

import br.com.abstratos.TemplateDeRelatorio;
import br.com.financeiro.Banco;
import br.com.financeiro.Conta;
import java.util.List;

public class RelatorioSimples extends TemplateDeRelatorio
{
    @Override
    protected void cabecalho(Banco banco) 
    {
        aparenciaRelatorio += "BANCO...:" + banco.getNome() + "\tTELEFONE...:" + banco.getTelefone() + "\n";
    }

    @Override
    protected void corpo(List<Conta> contas) 
    {
        for(Conta conta : contas) 
        {
            aparenciaRelatorio += "TITULAR...:" + conta.getTitular() + "\tSALDO...:" + conta.getSaldo() + "\n";
        }
    }

    @Override
    protected void rodape(Banco banco) 
    {
        aparenciaRelatorio += "BANCO...:" + banco.getNome() + "\t" + "TELEFONE...:" + banco.getTelefone();
    }
}
