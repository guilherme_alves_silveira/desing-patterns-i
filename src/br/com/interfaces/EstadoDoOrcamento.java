package br.com.interfaces;

import br.com.financeiro.Orcamento;

public interface EstadoDoOrcamento
{
    public void aplicaDescontoExtra(Orcamento orcamento);
    public void emAprovacao(Orcamento orcamento);
    public void aprovado(Orcamento orcamento);
    public void reprovado(Orcamento orcamento);
    public void finalizado(Orcamento orcamento);
}
