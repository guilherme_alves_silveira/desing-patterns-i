package br.com.interfaces;

import br.com.financeiro.Conta;

public interface EstadoDaConta
{
    public void saca(Conta conta, double valor);
    public void deposita(Conta conta, double valor);
}
