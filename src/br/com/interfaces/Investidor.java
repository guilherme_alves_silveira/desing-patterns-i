package br.com.interfaces;

public interface Investidor 
{
    public double valorInvestido(double valor);
}
