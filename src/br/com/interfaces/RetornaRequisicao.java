package br.com.interfaces;

import br.com.financeiro.Conta;
import br.com.financeiro.Requisicao;

public interface RetornaRequisicao 
{
    public String imprimeFormato(Requisicao requisicao, Conta conta);
    public void setProximo(RetornaRequisicao proximo);
}
