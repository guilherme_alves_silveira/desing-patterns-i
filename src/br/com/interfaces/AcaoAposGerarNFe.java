package br.com.interfaces;

import br.com.notafiscal.NotaFiscal;

public interface AcaoAposGerarNFe 
{
    public void executa(NotaFiscal nf);
}
