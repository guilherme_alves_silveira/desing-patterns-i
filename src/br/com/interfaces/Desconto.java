package br.com.interfaces;

import br.com.financeiro.Orcamento;

public interface Desconto 
{
    public double desconto(Orcamento orcamento);
    public void setProximo(Desconto proximo);
}
