package br.com.interfaces;

import br.com.enums.TipoRelatorio;
import br.com.financeiro.Banco;
import br.com.financeiro.Conta;
import java.util.List;

public interface Relatorio 
{
    public String RetornaRelatorio(Banco banco, List<Conta> contas, TipoRelatorio tipoRelatorio);
    public void setProximo(Relatorio proximo);
    public String relatorio(Banco banco, List<Conta> contas);
}
