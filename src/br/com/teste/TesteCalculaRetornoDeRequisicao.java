package br.com.teste;

import br.com.calculador.CalculaRetornoDeRequisicao;
import br.com.enums.Formato;
import br.com.financeiro.Conta;
import br.com.financeiro.Requisicao;

public class TesteCalculaRetornoDeRequisicao 
{
    public static void main(String[] args) 
    {
        CalculaRetornoDeRequisicao crdr = new CalculaRetornoDeRequisicao();
        Conta conta = new Conta();
        conta.setTitular("Oliveira");
        conta.deposita(1000.0);
        Requisicao r = new Requisicao(Formato.CSV);
        String retorno = crdr.RetornaTipoDaRequisicao(r, conta);
        
        System.out.println(retorno);
    }
}
