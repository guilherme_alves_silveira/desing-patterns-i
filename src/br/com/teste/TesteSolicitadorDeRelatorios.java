package br.com.teste;

import br.com.calculador.SolicitadorDeRelatorios;
import br.com.enums.TipoRelatorio;
import br.com.financeiro.Banco;
import br.com.financeiro.Conta;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TesteSolicitadorDeRelatorios
{
    public static void main(String[] args) 
    {
        Banco banco = new Banco();
        banco.setNome("BANCO BRADESCO");
        banco.setTelefone("(16)9999-9999");
        banco.setEndereco("TRAVESSA TROVÃO");
        banco.setEmail("BANCO@BRADESCO.COM.BR");
        
        Random nomeRandomico = new Random();
        Random atributoRandomico = new Random();
        
        String[] nomes = {"Ricardo", "Guilherme", "João", "Pedro", "Maria", "Larissa",
        "Jenifer", "Cristiane", "Carlos", "Enzo", "Matanza", "Julia", "Sofia", "Maria Eduarda", "Alice",
        "Laura", "Felipe", "Luiz Felipe", "Heitor", "Lorenzo", "Lucas", "Isac", "Vitor Hugo", "Vitor", 
        "Breno", "Brenda", "Diogo", "Otávio", "Sabrina", "Ana", "Nina", "Luciana", "Gabriela"
        };
        
        String[] sobreNomes = {"Madata" ,"Alves", "Ferreira", "Aligeri", "Verd", "Dante",
        "Souza", "Almeida", "Silva", "Silveira", "Eustacia"
        };
        
        String[] complemento = {"It", "Alb", "Ulb", "Bilb"};
        
        String[] numero = {"235321-" ,"453453-", "543523-", "459034-", "342321-", "342432-"};
        
        List<Conta> contas = new ArrayList<>();
        
        try 
        { 
            for(int i = 0;i < 50;i++)
            {
                Conta conta = new Conta();
                
                int rd = nomeRandomico.nextInt(33) * 1;
                int rd2 = nomeRandomico.nextInt(9) * 1;
                int at = atributoRandomico.nextInt(3) * 1;
                conta.setTitular(nomes[rd] + " " + sobreNomes[rd2] + " " + complemento[at]);
                
                at = atributoRandomico.nextInt(4) * 1;
                conta.setNumeroConta(numero[at] + at);
                
                at = atributoRandomico.nextInt(4) * 1;
                conta.setAgencia(numero[at]);
                
                contas.add(conta);
            }
        }
        catch(Exception err)
        {
            System.out.println("Ocorrência = " + err.getMessage());
        }
        
        SolicitadorDeRelatorios sdr = new SolicitadorDeRelatorios();
        System.out.println(sdr.solicitaRelatorioInformado(banco, contas, TipoRelatorio.SIMPLES));
    }
}
