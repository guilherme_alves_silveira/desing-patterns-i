package br.com.teste;

import br.com.calculador.CalculadorDeImpostos;
import br.com.financeiro.Item;
import br.com.financeiro.Orcamento;
import br.com.impostos.ICCC;
import br.com.impostos.ICMS;
import br.com.impostos.ICPP;
import br.com.impostos.IHIT;
import br.com.impostos.IKCV;
import br.com.impostos.ISS;
import br.com.abstratos.Imposto;

public class TesteCalculadorDeImpostos 
{
    public static void main(String[] args) 
    {
        CalculadorDeImpostos calculadorDeImposto = new CalculadorDeImpostos();
        
        Orcamento orcamento = new Orcamento(500.0);
        Orcamento orcamento2 = new Orcamento(1500.0);
        Orcamento orcamento3 = new Orcamento(3500.0);
        
        Imposto icms = new ICMS();
        Imposto iss = new ISS();
        Imposto iccc = new ICCC();
        
        calculadorDeImposto.CalculaImposto(orcamento, iss, "ISS");
        calculadorDeImposto.CalculaImposto(orcamento, icms, "ICMS");
        calculadorDeImposto.CalculaImposto(orcamento, iccc, "ICCC");
        calculadorDeImposto.CalculaImposto(orcamento2, iccc, "ICCC");
        calculadorDeImposto.CalculaImposto(orcamento3, iccc, "ICCC");
        
        Orcamento orcamento4 = new Orcamento(505.0);
        orcamento4.adicionaItem(new Item("PNEU",105.0));
        
        Imposto icpp = new ICPP();
        Imposto ikcv = new IKCV();
        Imposto ihit = new IHIT();
        calculadorDeImposto.CalculaImposto(orcamento4, icpp, "ICPP");
        calculadorDeImposto.CalculaImposto(orcamento4, ikcv, "IKCV");
        
        Orcamento orcamento5 = new Orcamento(505.0);
        orcamento5.adicionaItem(new Item("PNEU",105.0));
        orcamento5.adicionaItem(new Item("MADNESS",105.0));
        orcamento5.adicionaItem(new Item("TESTE",105.0));
        calculadorDeImposto.CalculaImposto(orcamento5, ihit, "IHIT");
    }
}
