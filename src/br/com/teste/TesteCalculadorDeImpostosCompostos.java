package br.com.teste;

import br.com.abstratos.Imposto;
import br.com.abstratos.TemplateDeImpostoCondicional;
import br.com.calculador.CalculadorDeImpostos;
import br.com.financeiro.Orcamento;
import br.com.impostos.IAV;
import br.com.impostos.ICCC;
import br.com.impostos.ICMS;
import br.com.impostos.ICPP;
import br.com.impostos.IKCV;

public class TesteCalculadorDeImpostosCompostos 
{
    public static void main(String[] args) 
    {
        CalculadorDeImpostos cdi = new CalculadorDeImpostos();
        
        Orcamento orcamento = new Orcamento(500);
        
        Imposto icmsComIcpp = new ICMS(new ICPP());
        Imposto icmsComIav = new ICMS(new IAV());
        TemplateDeImpostoCondicional ikcvComIccc = new IKCV(new ICCC());
        TemplateDeImpostoCondicional icppComIKCV = new ICPP(new IKCV());
        
        cdi.CalculaImposto(orcamento, icmsComIcpp, "ICMS com ICPP");
        cdi.CalculaImposto(orcamento, icmsComIav, "ICMS com IAV");
        cdi.CalculaImposto(orcamento, ikcvComIccc, "IKCV com ICCC");
        cdi.CalculaImposto(orcamento, icppComIKCV, "ICPP com IKCV");
    }
}
