package br.com.teste;

import br.com.calculador.CalculadorDeDescontos;
import br.com.financeiro.Item;
import br.com.financeiro.Orcamento;

public class TesteCalculadorDeDesconto 
{
    public static void main(String[] args) 
    {
        CalculadorDeDescontos cdd = new CalculadorDeDescontos();
        
        Orcamento orcamento = new Orcamento(500);
        Orcamento orcamento2 = new Orcamento(1500);
        
        Orcamento orcamento3 = new Orcamento(0);
        orcamento3.adicionaItem(new Item("Lápiz", 50));
        orcamento3.adicionaItem(new Item("Caneta", 50));
        orcamento3.adicionaItem(new Item("Azul", 50));
        orcamento3.adicionaItem(new Item("Vermelho", 50));
        orcamento3.adicionaItem(new Item("Marrom", 50));
        orcamento3.adicionaItem(new Item("Laranja", 50));
        
        Orcamento orcamento4 = new Orcamento(0);
        Item item = new Item("LAPIS", 100.0);
        Item casado = new Item("CANETA", 100.0);
        item.adicionaItemCasado(casado);
        orcamento4.adicionaItem(item);
        orcamento4.adicionaItem(casado);
        
        System.out.println("1 - Desconto : " + cdd.RetornaDesconto(orcamento));
        System.out.println("2 - Desconto : " + cdd.RetornaDesconto(orcamento2));
        System.out.println("3 - Desconto : " + cdd.RetornaDesconto(orcamento3));
        System.out.println("4 - Desconto : " + cdd.RetornaDesconto(orcamento4));
    }
}
