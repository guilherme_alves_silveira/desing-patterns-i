package br.com.teste;

import br.com.interfaces.AcaoAposGerarNFe;
import br.com.notafiscal.ItemDaNota;
import br.com.notafiscal.NotaFiscal;
import br.com.notafiscal.NotaFiscalBuilder;
import br.com.notafiscal.processosnfe.EnviadorDeEmail;
import br.com.notafiscal.processosnfe.EnviadorDeSms;
import br.com.notafiscal.processosnfe.Impressora;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TesteNotaFiscal 
{
    public static void main(String[] args) 
    {
        List<ItemDaNota> itens = Arrays.asList(new ItemDaNota("Item 1", 200), new ItemDaNota("Item 2", 300.0));
        List<AcaoAposGerarNFe> acoes = new ArrayList<>();
        acoes.add(new EnviadorDeEmail());
        acoes.add(new EnviadorDeSms());
        acoes.add(new Impressora());
        
        NotaFiscalBuilder builder = new NotaFiscalBuilder();
        builder.setRazaoSocial("RAZÃO SOCIAL")
        //.naData(new Calendar.Builder().setDate(2011, Calendar.MONTH, Calendar.DAY_OF_WEEK).build())
        .setCNPJ("000000000/000-0")
        .setObservacoes("OBSERVAÇÃO")
        .add(itens.get(0)).add(itens.get(1))
        .addAcoes(acoes);
        
        NotaFiscal notaFiscal = builder.constroi();
        
        System.out.println("RAZÃO : " + notaFiscal.getRazaoSocial());
        System.out.println("CNPJ : " + notaFiscal.getCNPJ());
        System.out.println("DATA : " + notaFiscal.getDataDeEmissao().getTime());
        System.out.println("IMPOSTOS : " + notaFiscal.getImpostos());
        
        for(ItemDaNota item : notaFiscal.getItens())
        {
            System.out.println(item.getDescricao() + " : " + item.getValor());
        }
        
        System.out.println("VALOR BRUTO : " + notaFiscal.getValorBruto());
        notaFiscal.executaAcaoAposGerarNFe();
    }
}
