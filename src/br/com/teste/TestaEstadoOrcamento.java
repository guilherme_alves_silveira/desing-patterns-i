package br.com.teste;

import br.com.financeiro.Orcamento;

public class TestaEstadoOrcamento 
{
    public static void main(String[] args) 
    {
        Orcamento orcamento = new Orcamento(500);
        
        orcamento.aplicaDesconto();
        System.out.println(orcamento.getValor());
        
        orcamento.aprova();
        orcamento.aplicaDesconto();
        System.out.println(orcamento.getValor());
    }
}
