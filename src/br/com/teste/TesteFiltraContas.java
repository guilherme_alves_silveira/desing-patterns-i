package br.com.teste;

import br.com.calculador.FiltraContas;
import br.com.financeiro.Conta;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;

public class TesteFiltraContas 
{
    public static void main(String[] args) 
    {
        Calendar dataRegular = Calendar.getInstance();
        dataRegular.set(1990, 4, 1);
        
        Calendar dataDesrregular = Calendar.getInstance();
        dataDesrregular.set(2015, 1, Calendar.DAY_OF_MONTH);
        
        FiltraContas fc = new FiltraContas();
        List<Conta> contas = new ArrayList<>();
        
        Conta c1 = new Conta();
        Conta c2 = new Conta();
        Conta c3 = new Conta();
        Conta c4 = new Conta();
        Conta c5 = new Conta();
        
        c1.setTitular("Larissa");
        c2.setTitular("Guilherme");
        c3.setTitular("Mendy");
        c4.setTitular("Marte Terrivel");
        c5.setTitular("Lua Meste");
        
        c1.deposita(50);
        c2.deposita(100);
        c3.deposita(150);
        c4.deposita(200);
        c5.deposita(500005);
        
        c1.setAberturaConta(dataRegular);
        c2.setAberturaConta(dataRegular);
        c3.setAberturaConta(dataRegular);
        c4.setAberturaConta(dataDesrregular);
        c5.setAberturaConta(dataRegular);
        
        contas.add(c1);
        contas.add(c2);
        contas.add(c3);
        contas.add(c4);
        contas.add(c5);

        Collection<Conta> filtrados = fc.FiltraAsContas(contas);
        for(Conta conta : filtrados)
        {
            System.out.println("Titular : " + conta.getTitular());
        }
    }
}
