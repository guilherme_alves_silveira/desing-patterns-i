package br.com.teste;

import br.com.financeiro.Conta;

public class TestaEstadoConta 
{
    public static void main(String[] args) 
    {
        Conta conta = new Conta();
        conta.setTitular("Guilherme");
        conta.setSaldo(-10);
        System.out.println("ESTADO = " + conta.getEstado());
        conta.setSaldo(100);
        System.out.println("ESTADO = " + conta.getEstado());
        System.out.println("VALOR = " + conta.getSaldo());
        
        conta.deposita(20);
        System.out.println("VALOR = " + conta.getSaldo());
        conta.setSaldo(-100);
        conta.deposita(110);
        System.out.println("VALOR = " + conta.getSaldo());
    }
}
