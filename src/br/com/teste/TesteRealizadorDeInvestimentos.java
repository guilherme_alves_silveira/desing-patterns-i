package br.com.teste;

import br.com.calculador.RealizadorDeInvestimentos;
import br.com.financeiro.Conta;
import br.com.interfaces.Investidor;
import br.com.invetidor.Arrojado;
import br.com.invetidor.Conservador;
import br.com.invetidor.Moderado;

public class TesteRealizadorDeInvestimentos 
{
    public static void main(String[] args) 
    {
        RealizadorDeInvestimentos realizadorDeInvestimentos = new RealizadorDeInvestimentos();
        
        Investidor conservador = new Conservador();
        Investidor moderado = new Moderado();
        Investidor arrojado = new Arrojado();
        
        double investimentoConservador = realizadorDeInvestimentos.RetornaInvestimento(conservador, 1000);
        double investimentoModerado = realizadorDeInvestimentos.RetornaInvestimento(moderado, 20000);
        double investimentoArrojado = realizadorDeInvestimentos.RetornaInvestimento(arrojado, 300000);
        
        Conta c1 = new Conta();
        Conta c2 = new Conta();
        Conta c3 = new Conta();
        
        c1.deposita(investimentoConservador);
        c2.deposita(investimentoModerado);
        c3.deposita(investimentoArrojado);
        
        System.out.println("Depositado : " + c1.getSaldo());
        System.out.println("Depositado : " + c2.getSaldo());
        System.out.println("Depositado : " + c3.getSaldo());
    }
}
