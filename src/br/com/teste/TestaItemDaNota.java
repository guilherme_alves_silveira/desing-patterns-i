package br.com.teste;

import br.com.notafiscal.ItemDaNota;
import br.com.notafiscal.ItemDaNotaBuilder;

public class TestaItemDaNota 
{
    public static void main(String[] args) 
    {
        ItemDaNotaBuilder builder = new ItemDaNotaBuilder();
        builder.setDescricao("Item 1")
        .setValor(200.0);
        
        ItemDaNota item = builder.constroi();
        System.out.println("DESCRIÇÃO : " + item.getDescricao());
        System.out.println("VALOR : " + item.getValor());
    }
}
