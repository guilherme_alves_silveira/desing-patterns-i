package br.com.invetidor;

import br.com.interfaces.Investidor;
import java.util.Random;

public class Moderado implements Investidor
{
    //"MODERADO", que tem 50% de chances de retornar 2.5%, e 50% de chances de retornar 0.7%;
    @Override
    public double valorInvestido(double valor)
    {
        Random random = new Random();
        boolean escolhido = random.nextDouble() >= 0.50;
        
        if(escolhido)
        {
            return valor * 0.025;
        }

        return valor * 0.007;
    }   
}
