package br.com.invetidor;

import br.com.interfaces.Investidor;
import java.util.Random;

public class Arrojado implements Investidor
{
    //"ARROJADO", que tem 20% de chances de retornar 5%, 
    // 30% de chances de retornar 3%, e 50% de chances de retornar 0.6%.
    @Override
    public double valorInvestido(double valor)
    {
        Random random  = new Random();
        boolean vinte  = random.nextDouble() < 0.20;
        boolean trinta = random.nextDouble() >= 0.20 && random.nextDouble() < 0.50;
        
        if(vinte)
        {
            return valor * 0.05;
        }
        else if(trinta)
        {
            return valor * 0.03;
        }

        return valor * 0.006;
    }
}
