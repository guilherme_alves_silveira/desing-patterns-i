package br.com.invetidor;

import br.com.interfaces.Investidor;

public class Conservador implements Investidor
{
    //"CONSERVADOR", que sempre retorna 0.8% do valor investido;
    @Override
    public double valorInvestido(double valor)
    {
        return valor * 0.008;
    }  
}
