package br.com.descontos;

import br.com.financeiro.Orcamento;
import br.com.interfaces.Desconto;

public class DescontoPraMaisDeQuinhentosValor implements Desconto
{
    private Desconto proximo;

    @Override
    public double desconto(Orcamento orcamento) 
    {
        if(orcamento.getValor() > 500)
        {
            return orcamento.getValor() * 0.07;
        }
        else
        {
            return proximo.desconto(orcamento);
        }
    }

    @Override
    public void setProximo(Desconto proximo)
    {
        this.proximo = proximo;
    }
}
