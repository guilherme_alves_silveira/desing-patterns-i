package br.com.descontos;

import br.com.financeiro.Item;
import br.com.financeiro.Orcamento;
import br.com.interfaces.Desconto;
import java.util.List;

public class DescontoPorVendaCasada implements Desconto
{
    private Desconto proximo;
    
    @Override
    public double desconto(Orcamento orcamento) 
    {
        boolean isCasado;
        
        for(Item item : orcamento.getItens())
        {
            isCasado = existe(item.getItensCasados(), orcamento);
            
            if(isCasado)
            {
                return orcamento.getValor() * 0.05;
            }
        }

        return proximo.desconto(orcamento);
    }

    @Override
    public void setProximo(Desconto proximo) 
    {
        this.proximo = proximo;
    }
    
    private boolean existe(List<Item> itensCasado, Orcamento orcamento) 
    {
        return orcamento.getItens().stream().anyMatch((item) -> (itensCasado.contains(item)));
    }   
}
