package br.com.descontos;

import br.com.financeiro.Orcamento;
import br.com.interfaces.Desconto;

public class SemDesconto implements Desconto
{
    @Override
    public double desconto(Orcamento orcamento) 
    {
        return 0;
    }

    @Override
    public void setProximo(Desconto proximo)
    {
        //Vazio
    }
}
