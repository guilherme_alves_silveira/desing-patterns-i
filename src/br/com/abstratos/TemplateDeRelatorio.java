package br.com.abstratos;

import br.com.enums.TipoRelatorio;
import br.com.financeiro.Banco;
import br.com.financeiro.Conta;
import br.com.interfaces.Relatorio;
import java.util.List;

public abstract class TemplateDeRelatorio implements Relatorio
{
    protected String aparenciaRelatorio;
    protected Relatorio proximo;
    
    @Override
    public String RetornaRelatorio(Banco banco, List<Conta> contas, TipoRelatorio tipoRelatorio)
    {
        if(tipoRelatorio.equals(TipoRelatorio.SIMPLES))
        {
            return relatorio(banco, contas);
        }
        else
        {
            return proximo.relatorio(banco, contas);
        }
    }
    
    protected abstract void cabecalho(Banco banco);

    protected abstract void corpo(List<Conta> contas);

    protected abstract void rodape(Banco banco);

    @Override
    public String relatorio(Banco banco, List<Conta> contas)
    {
        cabecalho(banco);
        corpo(contas);
        cabecalho(banco);
        return aparenciaRelatorio;
    }
    
    @Override
    public void setProximo(Relatorio proximo)
    {
        this.proximo = proximo;
    }
}
