package br.com.abstratos;

import br.com.financeiro.Conta;
import java.util.ArrayList;
import java.util.List;

public abstract class Filtro 
{
    protected Filtro proximo;
    
    public Filtro(){}
    
    public Filtro(Filtro proximo)
    {
        this.proximo = proximo;
    }
    
    public List<Conta> filtra(List<Conta> contas)
    {
        List<Conta> auditadas = new ArrayList<>();
        
        for(Conta conta : contas)
        {
            if(isValido(conta))
            {
                auditadas.add(conta);
            }
        }

        auditadas.addAll(filtraProximo(contas));
        
        return auditadas;
    }
    
    public List<Conta> filtraProximo(List<Conta> contas)
    {
        if(proximo != null ) return proximo.filtra(contas);
        else return new ArrayList<>();
    } 
    
    public abstract boolean isValido(Conta conta);
}
