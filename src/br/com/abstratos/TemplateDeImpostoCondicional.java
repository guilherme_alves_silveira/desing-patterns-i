package br.com.abstratos;

import br.com.financeiro.Orcamento;

public abstract class TemplateDeImpostoCondicional extends Imposto
{   
    public TemplateDeImpostoCondicional()
    {
        super();
    }
    
    public TemplateDeImpostoCondicional(Imposto outroImposto)
    {
        super(outroImposto);
    }
    
    @Override
    public final double CalculaImposto(Orcamento orcamento) 
    {
        if(deveUsarMaximaTaxacao(orcamento))
        {
            return maximaTaxacao(orcamento) + CalculaOutroImposto(orcamento);
        }
        else
        {
            return minimaTaxacao(orcamento) + CalculaOutroImposto(orcamento);
        }
    }

    protected abstract boolean deveUsarMaximaTaxacao(Orcamento orcamento);

    protected abstract double maximaTaxacao(Orcamento orcamento);
    
    protected abstract double minimaTaxacao(Orcamento orcamento);
}
