package br.com.abstratos;

import br.com.financeiro.Orcamento;

public abstract class Imposto 
{
    private final Imposto outroImposto;
    
    public Imposto()
    {
        this.outroImposto = null;
    }
    
    public Imposto(Imposto outroImposto)
    {
        this.outroImposto = outroImposto;
    }

    public abstract double CalculaImposto(Orcamento orcamento); 
    
    protected double CalculaOutroImposto(Orcamento orcamento)
    {
        return (outroImposto == null? 0 : outroImposto.CalculaImposto(orcamento));
    }
}
