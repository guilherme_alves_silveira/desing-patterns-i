package br.com.notafiscal;

import br.com.interfaces.AcaoAposGerarNFe;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class NotaFiscalBuilder 
{
    private String razaoSocial;
    private String CNPJ;
    private double valorBruto;
    private double impostos;
    private Calendar dataDeEmissao;
    private String observacoes;
    private final List<ItemDaNota> itens = new ArrayList<>();
    private List<AcaoAposGerarNFe> acoes;

    public NotaFiscalBuilder()
    {
        this.dataDeEmissao = Calendar.getInstance();
    }
    
    public NotaFiscalBuilder setRazaoSocial(String razaoSocial)
    {
        this.razaoSocial = razaoSocial;
        return this;
    }

    public NotaFiscalBuilder setCNPJ(String CNPJ) 
    {
        this.CNPJ = CNPJ;
        return this;
    }

    public NotaFiscalBuilder naData(Calendar dataDeEmissao) 
    {
        this.dataDeEmissao = dataDeEmissao;
        return this;
    }
    
    public NotaFiscalBuilder setObservacoes(String observacoes) 
    {
        this.observacoes = observacoes;
        return this;
    }

    public NotaFiscalBuilder add(ItemDaNota item)
    {
        this.itens.add(item);
        this.valorBruto += item.getValor();
        this.impostos += item.getValor() * 0.05;
        return this;
    }
    
    public NotaFiscalBuilder addAcoes(List<AcaoAposGerarNFe> acoes)
    {
        this.acoes = acoes;
        return this;
    }
        
    public NotaFiscal constroi()
    {
        return new NotaFiscal(razaoSocial, CNPJ, valorBruto,
                              impostos, dataDeEmissao, 
                              itens, observacoes, acoes);
    }
}
