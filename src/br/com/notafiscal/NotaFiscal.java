package br.com.notafiscal;

import br.com.interfaces.AcaoAposGerarNFe;
import java.util.Calendar;
import java.util.List;

public class NotaFiscal 
{
    private final String razaoSocial;
    private final String CNPJ;
    private final double valorBruto;
    private final double impostos;
    private final Calendar dataDeEmissao;
    private final String observacoes;
    private final List<ItemDaNota> itens;
    private final List<AcaoAposGerarNFe> acoes;
    
    public NotaFiscal(String razaoSocial, String CNPJ, double valorBruto, 
                      double impostos, Calendar dataDeEmissao,
                      List<ItemDaNota> itens, String observacoes, List<AcaoAposGerarNFe> acoes)
    {
        this.razaoSocial = razaoSocial;
        this.CNPJ = CNPJ;
        this.valorBruto = valorBruto;
        this.impostos = impostos;
        this.dataDeEmissao = dataDeEmissao;
        this.observacoes = observacoes;
        this.itens = itens;
        this.acoes = acoes;
    }

    public String getRazaoSocial() 
    {
        return razaoSocial;
    }

    public String getCNPJ() 
    {
        return CNPJ;
    }

    public double getValorBruto()
    {
        return valorBruto;
    }

    public double getImpostos() 
    {
        return impostos;
    }

    public Calendar getDataDeEmissao() 
    {
        return dataDeEmissao;
    }

    public String getObservacoes()
    {
        return observacoes;
    }

    public List<ItemDaNota> getItens() 
    {
        return itens;
    }
    
    public void executaAcaoAposGerarNFe()
    {
        for (AcaoAposGerarNFe acao : acoes) 
        {
            acao.executa(this);
        }
    }
}
