package br.com.notafiscal.processosnfe;

import br.com.interfaces.AcaoAposGerarNFe;
import br.com.notafiscal.NotaFiscal;

public class Multiplicador implements AcaoAposGerarNFe
{
    private final double fator;
    
    public Multiplicador(double fator) 
    {
        this.fator = fator;
    }
    
    @Override
    public void executa(NotaFiscal nf) 
    {
        System.out.println("Valor multiplicado : " + (nf.getValorBruto() * fator) );
    }
}
