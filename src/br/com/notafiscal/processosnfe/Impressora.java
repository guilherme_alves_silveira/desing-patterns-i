package br.com.notafiscal.processosnfe;

import br.com.interfaces.AcaoAposGerarNFe;
import br.com.notafiscal.NotaFiscal;

public class Impressora implements AcaoAposGerarNFe
{
    @Override
    public void executa(NotaFiscal nf)
    {
        System.out.println("Nota Fiscal Impressa.");
    }
}
