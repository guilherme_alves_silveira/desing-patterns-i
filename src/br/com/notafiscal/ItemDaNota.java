package br.com.notafiscal;

public class ItemDaNota 
{
    private final String descricao;
    private final double valor;
    
    public ItemDaNota(String descricao, double valor)
    {
        this.descricao = descricao;
        this.valor = valor;
    }

    public String getDescricao()
    {
        return descricao;
    }

    public double getValor() 
    {
        return valor;
    }
}
